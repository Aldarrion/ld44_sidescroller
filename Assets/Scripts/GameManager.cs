﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour
{
    public Text ElapsedTimeText;
    public GameObject EnemiesStoppedPanel;
    public Text HpText;
    public Text EggText;
    public Text[] SpellCountTexts;
    public Image[] SpellImages;

    [Space]
    public GameObject GameOverPanel;
    public Text GameOverMessage;
    public GameObject WinPanel;
    public Text WinText;
    public GameObject FirstLevelStartScreen;
    public GameObject Shop;
    public GameObject LevelFinishPanel;
    public Text[] SpellShopTexts;
    public Button[] SpellShopButtons;


    public static GameManager Instance { get; private set; }
    public static LevelManager LevelManager { get; private set; }

    public bool IsGameRunning { get; set; }
    private bool _isPlayerEnabled;
    public bool IsPlayerEnabled
    {
        get { return _isPlayerEnabled && IsGameRunning; }
        set { _isPlayerEnabled = value; }
    }
    private bool _areEnemiesEnabled;
    public bool AreEnemiesEnabled
    {
        get { return _areEnemiesEnabled && IsGameRunning; }
        set
        {
            _areEnemiesEnabled = value;
            EnemiesStoppedPanel.SetActive(!_areEnemiesEnabled);
        }
    }
    public GameObject Player { get; set; }

    public int CurrentLevel { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);

        LevelManager = GetComponent<LevelManager>();
        Debug.Assert(LevelManager);
        IsGameRunning = true;
    }

    private void Start()
    {
        //Util.IgnoreEnemyPlayerCollisions(true);
        //Util.IgnoreProjectileCollisions(true);

        StartLevel(0);
    }

    public void QuitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

    private void StopSimulation()
    {
        IsPlayerEnabled = false;
        AreEnemiesEnabled = false;
    }

    private void StartSimulation()
    {
        IsPlayerEnabled = true;
        AreEnemiesEnabled = true;
    }

    private void StartLevel(int number)
    {
        IsPlayerEnabled = false;
        CurrentLevel = number;
        LevelManager.LoadLevel(CurrentLevel);
        GameOverPanel.SetActive(false);
        WinPanel.SetActive(false);
        FirstLevelStartScreen.SetActive(false);
        Shop.SetActive(false);
        LevelFinishPanel.SetActive(false);

        AreEnemiesEnabled = true;

        if (CurrentLevel == 0)
        {
            Camera.main.transform.position = new Vector3(
                Player.transform.position.x,
                Player.transform.position.y,
                Camera.main.transform.position.z
            );
            OnCamTravelFinished();
        }
        else
        {
            Camera.main.GetComponent<CameraManager>().GoToPlayer(LevelManager.FinishXCoord);
        }
    }

    public void OnCamTravelFinished()
    {
        LevelManager.LoadLevel(CurrentLevel);
        StopSimulation();
        if (CurrentLevel == 0)
        {
            FirstLevelStartScreen.SetActive(true);
        }
        else
        {
            Shop.SetActive(true);
            UpdateShop();
        }
    }

    public void StartRun()
    {
        Shop.SetActive(false);
        FirstLevelStartScreen.SetActive(false);
        StartSimulation();
    }

    public void GameOver()
    {
        StopSimulation();

        GameOverPanel.SetActive(true);
        var hero = Player.GetComponent<HeroController>();
        GameOverMessage.text = $"You have managed to collect {hero.TotalEggCount} out of {hero.EggsToCollect} eggs.\nBetter luck next time!";
    }

    public void WinGame()
    {
        StopSimulation();
        WinPanel.SetActive(true);

        WinText.text = $"You have collected all the eggs in {ElapsedTimeText.text}.\nYou can play again and try different strategies to improve your time.";
    }

    public void RestartGame()
    {
        Destroy(Player);
        Player = null;
        StartLevel(0);
    }

    public void FinishLevel()
    {
        StopSimulation();
        LevelFinishPanel.SetActive(true);
    }

    public void StartNextLevel()
    {
        StartLevel((CurrentLevel + 1) % Maps.MapList.Length);
    }

    private void UpdateShop()
    {
        var hero = Player.GetComponent<HeroController>();
        foreach (SpellType spell in Enum.GetValues(typeof(SpellType)))
        {
            SpellShopButtons[(int)spell].interactable = hero.CanPurchase(spell);
            SpellData data = hero.GetData(spell);
            SpellShopTexts[(int)spell].text = $"Price: {data.Cost} HP\nCooldown: {data.Cooldown}s\nDuration: {data.Duration}s";
        }
    }

    public void BuyShield()
    {
        Player.GetComponent<HeroController>().BuySpell(SpellType.Shield);
        UpdateShop();
    }

    public void BuyTimeStop()
    {
        Player.GetComponent<HeroController>().BuySpell(SpellType.TimeStop);
        UpdateShop();
    }
}
