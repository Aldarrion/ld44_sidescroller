﻿using System;
using UnityEngine;

public enum TweenType
{
    Lerp,
    Cubic,
    Exp,
    Log,
    Sqrt
}

public static class Tweens
{
    public static float TweenByType(TweenType type, float a, float b, float t)
    {
        switch (type)
        {
            case TweenType.Lerp:
                return Lerp(a, b, t);
            case TweenType.Cubic:
                return Cubic(a, b, t);
            case TweenType.Exp:
                return Exp(a, b, t);
            case TweenType.Log:
                return Log(a, b, t);
            case TweenType.Sqrt:
                return Sqrt(a, b, t);
            default:
                throw new ArgumentOutOfRangeException(nameof(type), type, null);
        }
    }


    public static float Lerp(float a, float b, float t)
    {
        return a + (b - a) * t;
    }

    public static float Cubic(float a, float b, float t)
    {
        return a + (b - a) * t * t;
    }

    public static float Exp(float a, float b, float t)
    {
        return a + (b - a) * (Mathf.Exp(t) - 1);
    }

    public static float Log(float a, float b, float t)
    {
        return a + (b - a) * Mathf.Log(t + 1);
    }

    public static float Sqrt(float a, float b, float t)
    {
        return a + (b - a) * Mathf.Sqrt(t);
    }

    // TODO add tween from arbitrary unity curve
}
