﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Util
{
    public static readonly float UNIT = 2.56f;
    public static readonly int ROW_COUNT = 5;
    public static readonly int ROW_OFFSET = 6;

    private static readonly int PLAYER_LAYER = 8;
    private static readonly int ENEMY_LAYER = 9;
    private static readonly int PROJECTILE_LAYER = 10;

    public static void IgnoreEnemyPlayerCollisions(bool ignore)
    {
        Physics2D.IgnoreLayerCollision(PLAYER_LAYER, ENEMY_LAYER, ignore);
    }

    public static void IgnoreEnemyCollisions(bool ignore)
    {
        Physics2D.IgnoreLayerCollision(ENEMY_LAYER, ENEMY_LAYER, ignore);
    }

    public static void IgnoreProjectileCollisions(bool ignore)
    {
        Physics2D.IgnoreLayerCollision(PROJECTILE_LAYER, PROJECTILE_LAYER, ignore);
    }
}
