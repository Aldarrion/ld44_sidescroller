﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            var hero = collision.gameObject.GetComponent<HeroController>();
            hero.OnEggPickedUp();
            Destroy(gameObject);
        }
    }

    public void DestroyEgg()
    {
        // TODO add a particle effect or something
        Destroy(gameObject);
    }
}
