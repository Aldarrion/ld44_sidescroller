﻿using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public float Speed;
    public float FastSpeed;

    public bool IsGoingToPlayer;

    public void GoToPlayer(float startX)
    {
        GameObject player = GameManager.Instance.Player;
        Camera.main.transform.position = new Vector3(
            startX,
            player.transform.position.y,
            Camera.main.transform.position.z
        );

        IsGoingToPlayer = true;
    }

    void Update()
    {
        GameObject player = GameManager.Instance.Player;
        if (player == null)
            return;

        if (IsGoingToPlayer && Camera.main.transform.position.x > player.transform.position.x)
        {
            float newX = Camera.main.transform.position.x - (Input.GetKey(KeyCode.Space) ? FastSpeed : Speed) * Time.deltaTime;
            Camera.main.transform.position = new Vector3(
                newX,
                Camera.main.transform.position.y,
                Camera.main.transform.position.z
            );

            if (Camera.main.transform.position.x <= player.transform.position.x)
            {
                IsGoingToPlayer = false;
                GameManager.Instance.OnCamTravelFinished();
            }
        }
        else
        {
            Camera.main.transform.position = new Vector3(
                player.transform.position.x,
                Camera.main.transform.position.y,
                Camera.main.transform.position.z
            );
        }
    }
}
