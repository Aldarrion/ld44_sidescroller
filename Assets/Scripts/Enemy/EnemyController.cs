﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    protected SpriteRenderer _spriteRenderer;

    protected virtual void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        Debug.Assert(_spriteRenderer != null);
    }

    protected virtual void Update()
    {
        _spriteRenderer.sortingOrder = GetOrderInLayer(transform);
    }

    public static int GetOrderInLayer(Transform transform)
    {
        return (int)(transform.position.y * -100);
    }
}
