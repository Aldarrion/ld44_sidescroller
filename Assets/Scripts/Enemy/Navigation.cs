﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Priority_Queue;


class Navigation
{
    private static readonly int MAX_NODE_COUNT = 256;
    private static readonly Vector2Int[] NEIGHBOR_DIRS = new Vector2Int[]
    {
        new Vector2Int(1, 0),
        new Vector2Int(-1, 0),
        new Vector2Int(0, 1),
        new Vector2Int(0, -1),
    };

    public static Queue<Vector3> FindPath(Vector3 start, Vector3 end)
    {
        var intStart = new Vector2Int((int)(start.x / Util.UNIT), (int)(start.y / Util.UNIT));
        var intEnd = new Vector2Int((int)(end.x / Util.UNIT), (int)(end.y / Util.UNIT));
        List<Vector2Int> path = AStar(intStart, intEnd);

        Queue<Vector3> smoothPath = SmoothPath(path);

        return smoothPath;
    }

    private static List<Vector2Int> AStar(Vector2Int start, Vector2Int end)
    {
        if (start == end)
            return new List<Vector2Int>() { start };

        var frontier = new FastPriorityQueue<AStarNode>(MAX_NODE_COUNT);
        var startNode = new AStarNode(start);
        frontier.Enqueue(startNode, 0.0f);

        var cameFrom = new Dictionary<Vector2Int, Vector2Int>(23) { [start] = start };
        var costSoFar = new Dictionary<Vector2Int, int>(23) { [start] = 0 };
        var hexToNode = new Dictionary<Vector2Int, AStarNode>(23) { [start] = startNode };

        List<List<TileType>> map = GameManager.LevelManager.Map;

        bool isPathFound = false;
        while (frontier.Count > 0)
        {
            AStarNode current = frontier.Dequeue();
            if (current.Pos == end)
            {
                isPathFound = true;
                break;
            }

            foreach (Vector2Int dir in NEIGHBOR_DIRS)
            {
                var next = current.Pos + dir;
                if (map[next.y][next.x] == TileType.Full)
                    continue;

                var newCost = costSoFar[current.Pos] + 1;
                bool visited = costSoFar.TryGetValue(next, out int nextCostSoFar);
                if (!visited || newCost < nextCostSoFar)
                {
                    var prio = newCost + H(next, end);
                    if (visited)
                    {
                        frontier.UpdatePriority(hexToNode[next], prio);
                    }
                    else
                    {
                        var nextNode = new AStarNode(next);
                        frontier.Enqueue(nextNode, prio);
                        hexToNode[next] = nextNode;
                    }
                    costSoFar[next] = newCost;
                    cameFrom[next] = current.Pos;
                }
            }
        }

        var path = new List<Vector2Int>();
        if (!isPathFound)
        {
            return path;
        }

        Vector2Int curPathNode = end;
        do
        {
            path.Add(curPathNode);
            curPathNode = cameFrom[curPathNode];
        } while (curPathNode != start);

        // Always add start node to distinguish from empty (one node) paths
        path.Add(curPathNode);

        return path;
    }

    private static Queue<Vector3> SmoothPath(List<Vector2Int> path)
    {
        var smoothPath = new Queue<Vector3>();
        if (path.Count == 0)
            return smoothPath;

        Vector2Int last = path[path.Count - 1];
        smoothPath.Enqueue(ToWorldCoords(last));

        Vector2Int prev = last;
        for (int i = path.Count - 2; i >= 0; --i)
        {
            //Vector2Int next = path[i];
            //if (!IsClearStraight(last, next))
            //{
                //smoothPath.Enqueue(ToWorldCoords(prev));
                //last = prev;
            //}
            //prev = next;
            smoothPath.Enqueue(ToWorldCoords(path[i]));
        }

        return smoothPath;
    }

    private static Vector3 ToWorldCoords(Vector2Int point)
    {
        return new Vector3(point.x * Util.UNIT, point.y * Util.UNIT);
    }

    private static bool IsClearStraight(Vector2Int a, Vector2Int b)
    {
        return false;
    }

    private static float H(Vector2Int current, Vector2Int end)
    {
        return Mathf.Abs(current.x - end.x) + Mathf.Abs(current.y - end.y);
    }

    
    private class AStarNode : FastPriorityQueueNode
    {
        public Vector2Int Pos { get; set; }

        public AStarNode()
        {
            Pos = Vector2Int.zero;
        }

        public AStarNode(Vector2Int pos)
        {
            Pos = pos;
        }
    }
}

