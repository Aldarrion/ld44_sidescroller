﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class BombController : EnemyController
{
    public float ExplosionTimer;
    public int Damage;
    public float ExplosionRadius;
    public AudioClip[] ExplosionSounds;


    private bool _isTriggered;
    private float _timeToExplode;
    private bool _isPlayerNear;
    private bool _isExploded;
    private AudioSource _audioSource;

    protected override void Awake()
    {
        base.Awake();
        _audioSource = gameObject.AddComponent<AudioSource>();
        _audioSource.playOnAwake = false;
    }

    protected override void Update()
    {
        if (_isExploded)
            return;

        base.Update();

        if (!GameManager.Instance.AreEnemiesEnabled)
            return;

        if (_isPlayerNear && !_isTriggered)
        {
            _isTriggered = true;
            _timeToExplode = ExplosionTimer;
        }

        if (_isTriggered)
        {
            _spriteRenderer.color = Color.red;
            _timeToExplode -= Time.deltaTime;
            if (_timeToExplode <= 0)
            {
                Explode();
            }
        }
    }

    private void Explode()
    {
        _isExploded = true;
        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, ExplosionRadius, Vector2.zero);
        foreach (RaycastHit2D hit in hits)
        {
            GameObject hitGo = hit.collider.gameObject;
            if (hitGo.CompareTag("Player"))
            {
                hitGo.GetComponent<HeroController>().TakeDamage(Damage);
            }
            else if (hitGo.CompareTag("Egg"))
            {
                hitGo.GetComponent<EggController>().DestroyEgg();
            }
        }

        SoundUtil.PlayRandomSound(ExplosionSounds, _audioSource);
        _spriteRenderer.enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;

        int childCount = transform.childCount;
        for (int i = 0; i < childCount; ++i)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            _isPlayerNear = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            _isPlayerNear = false;
        }
    }
}
