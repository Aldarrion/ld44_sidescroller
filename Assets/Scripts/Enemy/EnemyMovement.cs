﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyMovement : MonoBehaviour
{
    public float Speed;

    public List<Vector3> Movement;

    Queue<Vector3> _path;
    Vector3? _nextNode;
    Vector3 _toNextNode;

    protected Rigidbody2D _rigidbody;

    public virtual void Awake()
    {
        //_enemyController = GetComponent<EnemyController>();
        _rigidbody = GetComponent<Rigidbody2D>();
        Debug.Assert(_rigidbody);
    }

    public virtual void Update()
    {
        if (!GameManager.Instance.AreEnemiesEnabled)
        {
            _rigidbody.velocity = Vector2.zero;
            return;
        }

        if (Movement?.Count > 1 && (_path == null || _path.Count == 0))
        {
            _path = new Queue<Vector3>(Movement);
        }

        if (_path?.Count > 0)
        {
            #region Debug
            Vector3 prev = _path.Peek();
            foreach (Vector3 v in _path.Skip(1))
            {
                Debug.DrawLine(prev, v, Color.red, 0, false);
                prev = v;
            }
            #endregion

            if (!_nextNode.HasValue || Vector3.Distance(transform.position, _nextNode.Value) < 0.05f)
            {
                _nextNode = _path.Dequeue();
                _toNextNode = _nextNode.Value - transform.position;
                _toNextNode.Normalize();
            }

            _rigidbody.velocity = _toNextNode * Speed;
        }

        if (_nextNode.HasValue)
        {
            Debug.DrawLine(transform.position, _nextNode.Value, Color.green, 0, false);
        }
    }


}
