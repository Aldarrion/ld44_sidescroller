﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeController : MonoBehaviour
{
    CircleCollider2D _eyeCircle;
    Transform _eye;
    SpriteRenderer _eyeRenderer;

    private void Awake()
    {
        _eyeCircle = GetComponent<CircleCollider2D>();
        _eye = transform.GetChild(0);
        _eyeRenderer = _eye.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        Vector3 playerPos = GameManager.Instance.Player.transform.position;
        Vector3 toPlayer = playerPos - transform.position;
        Vector2 toPlayerNormalized = new Vector2(toPlayer.x, toPlayer.y).normalized;
        _eye.localPosition = new Vector3(
            _eyeCircle.radius * toPlayerNormalized.x,
            _eyeCircle.radius * toPlayerNormalized.y,
            _eye.localPosition.z
        );

        _eyeRenderer.sortingOrder = EnemyController.GetOrderInLayer(transform.parent) + 1;
    }
}
