﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SimpleMovement : EnemyMovement
{
    public override void Awake()
    {
        base.Awake();
    }

    Queue<Vector3> _path;

    Vector3? _nextNode;
    Vector3 _toNextNode;

    public override void Update()
    {
        base.Update();

        if (_path == null || _path.Count < 1)
        {
            _toNextNode = Vector3.zero;

            var player = GameManager.Instance.Player;
            if (Vector3.Distance(player.transform.position, transform.position) > 1.0f)
            {
                _path = Navigation.FindPath(transform.position, player.transform.position);
                _path.Dequeue();
            }
            else
            {
                _path = null;
            }
        }

        if (_path?.Count > 0)
        {
            #region Debug
            Vector3 prev = _path.Peek();
            foreach (Vector3 v in _path.Skip(1))
            {
                Debug.DrawLine(prev, v, Color.red, 0, false);
                prev = v;
            }
            #endregion

            if (!_nextNode.HasValue || Vector3.Distance(transform.position, _nextNode.Value) < 0.05f)
            {
                _nextNode = _path.Dequeue();
                _toNextNode = _nextNode.Value - transform.position;
                _toNextNode.Normalize();
            }

            _rigidbody.velocity = _toNextNode * Speed;
        }

        if (_nextNode.HasValue)
        {
            Debug.DrawLine(transform.position, _nextNode.Value, Color.green, 0, false);
        }
    }
}
