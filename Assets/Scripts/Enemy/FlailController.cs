﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class FlailController : EnemyController
{
    public int Damage;

    public float DamageCooldown;

    private bool _isPlayerNear;

    private float _dmgCooldown;

    protected override void Update()
    {
        base.Update();

        _dmgCooldown -= Time.deltaTime;

        if (_dmgCooldown <= 0 && _isPlayerNear)
        {
            GameManager.Instance.Player.GetComponent<HeroController>().TakeDamage(Damage);

            _dmgCooldown = DamageCooldown;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            _isPlayerNear = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            _isPlayerNear = false;
        }
    }
}
