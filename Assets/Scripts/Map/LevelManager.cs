﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public enum TileType
{
    Empty,
    Full
}

public enum MapTileType
{
    Wall,
    Floor,
    Egg,
    PlayerSpawn,
    EnemyBomb,
    EnemyFlail,
    Finish
}

public class LevelManager : MonoBehaviour
{
    public GameObject WallPrefab;
    public GameObject FloorPrefab;
    public GameObject EnemyBombPrefab;
    public GameObject EnemyFlailPrefab;
    public GameObject PlayerPrefab;
    public GameObject EggPrefab;
    public GameObject FinishPrefab;

    public float FinishXCoord { get; private set; }

    public List<List<TileType>> Map { get; private set; }

    public void LoadLevel(int number)
    {
        DestroyOldLevel();

        Map = new List<List<TileType>>();
        string[] mapStr = Maps.MapList[number].Reverse().ToArray();

        bool isPlayerSpawned = false;
        for (int mapPos = 0; mapPos < mapStr.Length; mapPos++)
        {
            string lineStr = mapStr[mapPos];
            var row = new List<TileType>();
            for (int charPos = 0; charPos < lineStr.Length; charPos++)
            {
                char c = lineStr[charPos];
                MapTileType mapTile = MakeTile(c);
                if (mapTile == MapTileType.PlayerSpawn)
                {
                    if (isPlayerSpawned)
                        Debug.LogError("Multiple player spawns detected");
                    isPlayerSpawned = true;
                }
                GameObject gameObject = CreateTile(mapTile, new Vector2Int(charPos, mapPos));
                if (mapTile == MapTileType.EnemyFlail)
                {
                    SetEnemyFlailStartAndEnd(gameObject, mapPos, charPos, mapStr);
                }

                row.Add(MakeTile(mapTile));
            }
            Map.Add(row);
        }

        if (!isPlayerSpawned)
            Debug.LogError("No player spawn found in map");
    }

    private void SetEnemyFlailStartAndEnd(GameObject gameObject, int mapPos, int charPos, string[] mapStr)
    {
        EnemyMovement enemyMovement = gameObject.GetComponent<EnemyMovement>();
        enemyMovement.Movement = new List<Vector3>();

        enemyMovement.Movement.Add(new Vector3(charPos * Util.UNIT, mapPos * Util.UNIT));

        Vector2Int direction = new Vector2Int();

        char c = mapStr[mapPos][charPos];

        switch (c)
        {
            case '↑':
                direction.y = 1;
                break;
            case '↓':
                direction.y = -1;
                break;
            case '→':
                direction.x = 1;
                break;
            case '←':
                direction.x = -1;
                break;
            default:
                return;
        }

        int x = charPos;
        int y = mapPos;

        while (mapStr[y][x] != 'O')
        {
            x += direction.x;
            y += direction.y;

            if (y < 0 || y >= mapStr.Length || x < 0 || x >= mapStr[y].Length)
            {
                throw new System.Exception("End not found for flail row " + mapPos + " column " + charPos);
            }
        }

        enemyMovement.Movement.Add(new Vector3(x * Util.UNIT, y * Util.UNIT));
    }

    private void DestroyOldLevel()
    {
        int count = transform.childCount;
        for (int i = 0; i < count; ++i)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    private GameObject CreateTile(MapTileType tileType, Vector2Int pos)
    {
        var realPos = new Vector3(pos.x * Util.UNIT, pos.y * Util.UNIT);
        switch (tileType)
        {
            case MapTileType.Wall:
                CreateTile(MapTileType.Floor, pos);
                return Instantiate(WallPrefab, realPos, Quaternion.identity, transform);
            case MapTileType.Floor:
                return Instantiate(FloorPrefab, realPos, Quaternion.identity, transform);
            case MapTileType.EnemyBomb:
                CreateTile(MapTileType.Floor, pos);
                return Instantiate(EnemyBombPrefab, realPos, Quaternion.identity, transform);
            case MapTileType.EnemyFlail:
                CreateTile(MapTileType.Floor, pos);
                return Instantiate(EnemyFlailPrefab, realPos, Quaternion.identity, transform);
            case MapTileType.PlayerSpawn:
                CreateTile(MapTileType.Floor, pos);
                GameObject player = GameManager.Instance.Player;
                if (player == null)
                {
                    GameManager.Instance.Player = Instantiate(PlayerPrefab, realPos, Quaternion.identity);
                }
                else
                {
                    player.transform.position = realPos;
                    player.GetComponent<HeroController>().StartNewLevel();
                }
                return player;
            case MapTileType.Egg:
                CreateTile(MapTileType.Floor, pos);
                return Instantiate(EggPrefab, realPos, Quaternion.identity, transform);
            case MapTileType.Finish:
                GameObject gameObject = Instantiate(FinishPrefab, realPos, Quaternion.identity, transform);
                FinishXCoord = realPos.x;
                return gameObject;
        }
        throw new System.Exception("Unknown tileType " + tileType);
    }

    private MapTileType MakeTile(char c)
    {
        switch (c)
        {
            case '#':
                return MapTileType.Wall;
            case 'P':
                return MapTileType.PlayerSpawn;
            case 'b':
                return MapTileType.EnemyBomb;
            case 'f':
                return MapTileType.EnemyFlail;
            case '↑':
                return MapTileType.EnemyFlail;
            case '↓':
                return MapTileType.EnemyFlail;
            case '→':
                return MapTileType.EnemyFlail;
            case '←':
                return MapTileType.EnemyFlail;
            case 'O':
                // flail end
                return MapTileType.Floor;
            case 'e':
                return MapTileType.Egg;
            case 'X':
                return MapTileType.Finish;
            default:
                return MapTileType.Floor;
        }
    }

    private TileType MakeTile(MapTileType t)
    {
        switch (t)
        {
            case MapTileType.Wall:
                return TileType.Full;
            default:
                return TileType.Empty;
        }
    }
}
