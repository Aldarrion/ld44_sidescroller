﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour
{
    public int EggsToCollect;
    public int StartHP;
    public int EggsForHP;
    public int HPForEggs;
    public float OnDamageInvulnurableTime;
    public float GlobalCooldown;
    public GameObject FloatingTextPrefab;
    public GameObject ShieldObject;
    public SpellData[] SpellsData;
    public AudioClip[] EggPickupSounds;
    public AudioClip[] DamageTakenSounds;
    public AudioClip[] ShieldSounds;
    public AudioClip[] TimeStopSounds;
    public AudioClip[] HealSounds;

    public float InvulnurableDuration { get; private set; }
    public bool IsInvulnerable => InvulnurableDuration > 0;

    private int _currentHp;
    public int CurrentHP
    {
        get
        {
            return _currentHp;
        }
        set
        {
            _currentHp = value;
            GameManager.Instance.HpText.text = _currentHp.ToString();
        }
    }

    private int _totalEggCount;
    public int TotalEggCount
    {
        get { return _totalEggCount; }
        set
        {
            _totalEggCount = value;
            GameManager.Instance.EggText.text = $"{_totalEggCount.ToString()}/{EggsToCollect}";
        }
    }

    private TimeSpan _elapsedTime;
    public TimeSpan ElapsedTime
    {
        get { return _elapsedTime; }
        set
        {
            _elapsedTime = value;
            GameManager.Instance.ElapsedTimeText.text = _elapsedTime.ToString(@"mm\:ss\.ff");
        }
    }

    private int[] _spellCounts;

    private readonly List<Spell> _spells = new List<Spell>();

    private Animator _animator;
    private AudioSource _damageAudioSrc;
    private AudioSource _eggPickAudioSrc;
    private AudioSource _healAudioSrc;
    private AudioSource _shieldAudioSrc;
    private AudioSource _timeStopAudioSrc;

    public SpellData GetData(SpellType type) => SpellsData[(int)type];

    public bool CanPurchase(SpellType type) => GetData(type).Cost < CurrentHP;

    public bool IsSpellAvailable(SpellType type) => _spellCounts[(int)type] > 0;

    public void MakeInvulnerable(float duration)
    {
        InvulnurableDuration = Mathf.Max(InvulnurableDuration, duration);
    }

    public void OnSpellActivated(SpellType type)
    {
        ModifySpellCount(type, -1);
        foreach(Spell s in _spells)
        {
            s.ActivateGCD(GlobalCooldown);
        }

        if (type == SpellType.Shield)
            SoundUtil.PlayRandomSound(ShieldSounds, _shieldAudioSrc);
        else
            SoundUtil.PlayRandomSound(TimeStopSounds, _timeStopAudioSrc);
    }

    public void StartNewLevel()
    {
        InvulnurableDuration = 0;
        foreach (Spell s in _spells)
            s.ResetSpell();

        GetComponent<HeroMovement>().StartNewLevel();
    }

    private void Awake()
    {
        _animator = GetComponent<Animator>();

        _damageAudioSrc = gameObject.AddComponent<AudioSource>();
        _damageAudioSrc.playOnAwake = false;

        _eggPickAudioSrc = gameObject.AddComponent<AudioSource>();
        _eggPickAudioSrc.playOnAwake = false;

        _healAudioSrc = gameObject.AddComponent<AudioSource>();
        _healAudioSrc.playOnAwake = false;

        _shieldAudioSrc = gameObject.AddComponent<AudioSource>();
        _shieldAudioSrc.playOnAwake = false;

        _timeStopAudioSrc = gameObject.AddComponent<AudioSource>();
        _timeStopAudioSrc.playOnAwake = false;

        CurrentHP = StartHP;
    }

    private void Start()
    {
        foreach (SpellType spell in Enum.GetValues(typeof(SpellType)))
        {
            _spells.Add(Spell.MakeSpell(spell, GetData(spell), this));
        }
        
        TotalEggCount = 0;

        _spellCounts = new int[Enum.GetValues(typeof(SpellType)).Length];
        ModifySpellCount(SpellType.Shield, 3);
        ModifySpellCount(SpellType.TimeStop, 2);
        ElapsedTime = new TimeSpan(0);
    }

    private void Update()
    {
        if (!GameManager.Instance.IsPlayerEnabled)
        {
            _animator.enabled = false;
            return;
        }

        _animator.enabled = true;

        ElapsedTime += TimeSpan.FromSeconds(Time.deltaTime);

        if (InvulnurableDuration > 0)
            InvulnurableDuration -= Time.deltaTime;

        foreach (Spell spell in _spells)
        {
            spell.Update();
        }
    }

    public void BuySpell(SpellType type)
    {
        if (CanPurchase(type))
        {
            SpellData data = GetData(type);
            CurrentHP -= data.Cost;
            ModifySpellCount(type, 1);
        }
    }

    public void SetShieldActive(bool isActive)
    {
        ShieldObject.SetActive(isActive);
    }

    public void AddHp(int hp)
    {
        CurrentHP += hp;
        var hpText = Instantiate(FloatingTextPrefab, transform.position, Quaternion.identity).GetComponent<FlyingTextController>();
        hpText.SetHp(hp);
        SoundUtil.PlayRandomSound(HealSounds, _healAudioSrc);
    }

    public void ModifySpellCount(SpellType type, int count)
    {
        _spellCounts[(int)type] += count;
        GameManager.Instance.SpellCountTexts[(int)type].text = $"{_spellCounts[(int)type]}x";
    }

    public void OnEggPickedUp()
    {
        SoundUtil.PlayRandomSound(EggPickupSounds, _eggPickAudioSrc);
        ++TotalEggCount;
        if (TotalEggCount % EggsForHP == 0)
        {
            AddHp(HPForEggs);
        }
        if (TotalEggCount >= EggsToCollect)
        {
            GameManager.Instance.WinGame();
        }
    }

    public void TakeDamage(int damage)
    {
        if (IsInvulnerable)
            return;

        SoundUtil.PlayRandomSound(DamageTakenSounds, _damageAudioSrc);
        var pos = new Vector3(transform.position.x, transform.position.y, -9);
        var hpText = Instantiate(FloatingTextPrefab, pos, Quaternion.identity).GetComponent<FlyingTextController>();
        hpText.SetHp(-damage);

        CurrentHP -= damage;
        if (CurrentHP <= 0)
        {
            GameManager.Instance.GameOver();
            return;
        }
    }
}
