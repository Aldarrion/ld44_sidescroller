﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public enum SpellType
{
    Shield,
    TimeStop
}

[CreateAssetMenu(fileName = "SpellData", menuName = "Scroller/SpellData", order = 2)]
public class SpellData : ScriptableObject
{
    public string Name;

    [TextArea]
    public string Description;
    public int Cost;
    public float Cooldown;
    public bool IgnoresGCD;
    public float Duration;
}

public abstract class Spell
{
    protected SpellData Data;
    protected float CooldownInProgress;
    private float _remainingCooldown;
    protected float RemainingCooldown
    {
        get { return _remainingCooldown; }
        set
        {
            _remainingCooldown = value;
            ImageMaterial.SetFloat(ProgressId, 1 - _remainingCooldown / CooldownInProgress);
        }
    }
    protected float RemainingDuration;
    protected HeroController Hero;
    protected KeyCode Key;
    protected Material ImageMaterial;
    protected int ProgressId;

    protected abstract SpellType Type { get; }

    public virtual void Init()
    {
        ImageMaterial = GameObject.Instantiate(GameManager.Instance.SpellImages[(int)Type].material);
        ProgressId = Shader.PropertyToID("_Progress");
        GameManager.Instance.SpellImages[(int)Type].material = ImageMaterial;
        RemainingCooldown = 0;
    }

    public virtual void Update()
    {
        if (RemainingDuration > 0)
        {
            RemainingDuration -= Time.deltaTime;
            if (RemainingDuration <= 0)
            {
                Uncast();
            }
        }

        if (RemainingCooldown > 0)
            RemainingCooldown -= Time.deltaTime;

        if (RemainingCooldown <= 0 
            && Input.GetKeyDown(Key) 
            && Hero.IsSpellAvailable(Type))
        {
            RemainingDuration = Data.Duration;
            ActivateCooldown(Data.Cooldown);
            Hero.OnSpellActivated(Type);
            Cast();
        }
    }

    protected abstract void Cast();
    protected abstract void Uncast();

    public virtual void ActivateGCD(float cooldown)
    {
        if (!Data.IgnoresGCD)
        {
            ActivateCooldown(cooldown);
        }
    }

    public virtual void ActivateCooldown(float cooldown)
    {
        if (RemainingCooldown < cooldown)
        {
            RemainingCooldown = cooldown;
            CooldownInProgress = cooldown;
        }
    }

    public virtual void ResetSpell()
    {
        RemainingCooldown = 0;
        Uncast();
    }

    public static Spell MakeSpell(SpellType type, SpellData data, HeroController hero)
    {
        Spell s;
        switch (type)
        {
            case SpellType.Shield:
            {
                s = new ShieldSpell
                {
                    Key = KeyCode.Q
                };
                break;
            }
            case SpellType.TimeStop:
            {
                s = new TimeStopSpell
                {
                    Key = KeyCode.E
                };
                break;
            }
            default:
            {
                Debug.LogError("Spell not implemented yet");
                return null;
            }
        }

        s.Data = data;
        s.Hero = hero;
        s.Init();

        return s;
    }
}

public class ShieldSpell : Spell
{
    protected override SpellType Type => SpellType.Shield;

    protected override void Cast()
    {
        Hero.MakeInvulnerable(Data.Duration);
        Hero.SetShieldActive(true);
    }

    protected override void Uncast()
    {
        Hero.SetShieldActive(false);
    }
}

public class TimeStopSpell : Spell
{
    protected override SpellType Type => SpellType.TimeStop;

    protected override void Cast()
    {
        GameManager.Instance.AreEnemiesEnabled = false;
    }

    protected override void Uncast()
    {
        GameManager.Instance.AreEnemiesEnabled = true;
    }
}
