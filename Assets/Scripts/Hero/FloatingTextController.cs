﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingTextController : MonoBehaviour
{
    public float FadeOutTime;
    public Color NegativeHpColor;
    public Color PositiveHpColor;

    private TextMesh _text;
    private float _fadeOutTime;

    private void Awake()
    {
        _text = GetComponent<TextMesh>();
    }

    public void SetHp(int hp)
    {
        _text.text = hp.ToString();
        if (hp < 0)
            _text.color = NegativeHpColor;
        else
            _text.color = PositiveHpColor;
    }

    private void Update()
    {
        if (_fadeOutTime <= 0)
        {
            Destroy(gameObject);
        }

        float a = Tweens.Exp(0, 1, _fadeOutTime / FadeOutTime);
        _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, a);

        _fadeOutTime -= Time.deltaTime;
    }
}
