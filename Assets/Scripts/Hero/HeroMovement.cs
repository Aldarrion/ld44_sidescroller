﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class HeroMovement : MonoBehaviour
{
    public float Speed;
    public float FastSpeed;
    public float LineSwitchSpeed;

    private int _line;
    private Rigidbody2D _rigidBody;
    private AudioSource _audioSource;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        Debug.Assert(_rigidBody != null);
        _audioSource = GetComponent<AudioSource>();
        Debug.Assert(_audioSource != null);

        _line = (int)(transform.position.y / Util.UNIT);
    }

    public void StartNewLevel()
    {
        _line = (int)(transform.position.y / Util.UNIT);
    }

    void Update()
    {
        if (!GameManager.Instance.IsPlayerEnabled)
        {
            _rigidBody.velocity = Vector2.zero;
            //if (_audioSource.isPlaying)
                //_audioSource.Stop();
        }
        else
        {
            //if (!_audioSource.isPlaying)
                //_audioSource.Play();
            bool down = Input.GetButtonDown("Down");
            bool up = Input.GetButtonDown("Up");
            int vertMovement = down ? -1 : up ? 1 : 0;
            if (vertMovement != 0)
            {
                _line = Mathf.Clamp(_line + (int)Mathf.Sign(vertMovement), Util.ROW_OFFSET, Util.ROW_OFFSET + Util.ROW_COUNT - 1);
            }

            float currY = transform.position.y;
            float desiredY = _line * Util.UNIT;
            var side = Vector3.zero;
            if (Mathf.Abs(desiredY - currY) > 0.5)
            {
                side = Vector2.up * Mathf.Sign(desiredY - currY) * LineSwitchSpeed;
            }
            else if (Mathf.Abs(desiredY - currY) > 0.1) // So we do not overshoot all the time
            {
                side = Vector2.up * Mathf.Sign(desiredY - currY) * LineSwitchSpeed / 4;
            }
            else
            {
                _rigidBody.position = new Vector3(transform.position.x, desiredY, transform.position.z);
            }

             var forward = Vector3.right * (Input.GetKey(KeyCode.Space) ? FastSpeed : Speed);
            
            _rigidBody.velocity = forward + side;
        }

    }
}
