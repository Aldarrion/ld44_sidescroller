﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class FlyingTextController : MonoBehaviour
{
    public float FullFadeInSeconds;
    public float FlySpeed;
    public TweenType FadeTweenType;
    public Color NegativeHpColor;
    public Color PositiveHpColor;

    public void SetText(string text)
    {
        _text.text = text;
    }

    public void SetTextColor(Color color)
    {
        _text.color = color;
    }

    public void SetHp(int hp)
    {
        if (hp < 0)
        {
            _text.text = hp.ToString();
            _text.color = NegativeHpColor;
        }
        else
        {
            _text.text = $"+{hp}";
            _text.color = PositiveHpColor;
        }
    }

    #region Private

    private TextMesh _text;

    private float _alphaProgress = 0.0f;

    private void Awake()
    {
        _text = GetComponentInChildren<TextMesh>();
        GetComponent<TextMesh>().GetComponent<MeshRenderer>().sortingLayerName = "UI";
    }

    private void Update()
    {
        if (_text.color.a < 0.2f)
        {
            Destroy(gameObject);
            return;
        }

        _alphaProgress += Time.deltaTime / FullFadeInSeconds;
        _text.color = new Color(
            _text.color.r,
            _text.color.g,
            _text.color.b,
            Tweens.TweenByType(FadeTweenType, 1.0f, 0.0f, _alphaProgress)
        );

        transform.position = new Vector3(
            transform.position.x,
            transform.position.y + (Time.deltaTime * FlySpeed),
            transform.position.z
        );
    }

    #endregion
}
