﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharacterMovement : MonoBehaviour
{
    public float Speed;

    private Rigidbody2D _rigidBody;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        Debug.Assert(_rigidBody != null);
    }

    void Update()
    {
        if (!GameManager.Instance.IsPlayerEnabled)
        {
            _rigidBody.velocity = Vector2.zero;
            
        }
        else
        {
            

            Vector2 velocity = _rigidBody.velocity;
            velocity.x = Speed * Input.GetAxis("Horizontal");
            velocity.y = Speed * Input.GetAxis("Vertical");

            _rigidBody.velocity = velocity;
        }

    }
}
