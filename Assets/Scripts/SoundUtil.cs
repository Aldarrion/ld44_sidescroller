﻿using System;
using System.Collections.Generic;

using UnityEngine;

public static class SoundUtil
{
    public static void PlayRandomSound(IReadOnlyList<AudioClip> sounds, AudioSource source)
    {
        AudioClip sound = sounds[UnityEngine.Random.Range(0, sounds.Count)];
        source.clip = sound;
        source.Play();
    }
}